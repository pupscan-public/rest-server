# Pupscan Rest API Definition

## Introduction

The purpose of this API is to enable sending multiple files with json metadata to a web server.  
The pup is using a very simple protocol based on HTTP multipart.  
This code is there to demonstrate server side implementation and is providing a client tool to verify the implementation.

## Install 

```bash
yarn install
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

## Run server

```bash
yarn server
```

## Browser client

Open URL 
http://localhost:3000

## CLI client

```bash
sh client.sh
```

## About the protocol

We are using the multipart protocol for encoding the Document **Header** metadata together with the **File** binaries.
The metadata is under the form-data with ```name="json"```.
The files are stored under multiple form-data with ```name="file"```.
The binding between the json and the file is made using the key ```filename="image.jpg"```.


**HTTP Request description :**

```
POST /fileupload/upload HTTP/1.1
Host: localhost:8080
Content-Type: multipart/form-data;
boundary=---------------------------263081694432439
Content-Length: 4413245
-----------------------------263081694432439
Content-Disposition: form-data; name="json"

... JSON CONTENT HERE ...
-----------------------------263081694432439
Content-Disposition: form-data; name="file"; filename="image.jpg"
Content-Type: image/jpeg

... FILE 1 DATA HERE ...
-----------------------------263081694432439
Content-Disposition: form-data; name="file"; filename="file.txt"
Content-Type: text/plain

... FILE 2 DATA HERE ...
-----------------------------263081694432439--
```