var fs = require('fs'),
    formidable = require('formidable'),
    http = require('http'),
    util = require('util'),
    os = require('os'),
    port = 3000,
    uploadDir = 'uploads',
    server;

server = http.createServer(function (req, res) {
    if (req.url == '/') {
        res.writeHead(200, {'content-type': 'text/html'});
        res.end(
            '<form action="/upload" enctype="multipart/form-data" method="post">' +
            '<textarea name="json" cols="40" rows="5">{ "version" : "1.2.3" }</textarea><br>' +
            '<input type="file" name="file" multiple="multiple"><br>' +
            '<input type="submit" value="Upload">' +
            '</form>'
        );
    } else if (req.url == '/delete') {
        rmdirs(uploadDir)
        fs.mkdirSync(uploadDir)
        res.writeHead(200, {'content-type': 'text/plain'});
        res.end('Deleted')
    } else if (req.url == '/upload') {
        var form = new formidable.IncomingForm(),
            files = [],
            json = null;

        form.maxFileSize = 100 * 1024 * 1024; //100MB
        form.uploadDir = uploadDir

        form
            .on('field', function (field, value) {
                if (field == 'json') {
                    console.log('> Json received');
                    json = JSON.parse(value);
                }
            })
            .on('file', function (field, file) {
                console.log('> File received "'+field+'":"'+file.name+'"');
                files.push(file);
            })
            .on('end', function () {
                res.writeHead(200, {'content-type': 'text/plain'});
                res.write('Json version : ' + json.version + '\n');
                res.write(files.length + ' files received.' + '\n');
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    res.write('[' + i + '] ' + file.name + '\n');
                    fs.renameSync(file.path,form.uploadDir+'/'+file.name)
                    //console.log(util.inspect(file))
                }
                console.log('> SUCCESS');
                res.end();
            });
        form.parse(req);
    } else {
        res.writeHead(404, {'content-type': 'text/plain'});
        res.end('404');
    }
});
server.listen(port);

console.log('listening on http://localhost:' + port + '/');
console.log('to clean uploaded files use http://localhost:' + port + '/delete');

rmdirs = function(path) {
    var files = [];
    if( fs.existsSync(path) ) {
        files = fs.readdirSync(path);
        files.forEach(function(file,index){
            var curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};